\version "2.19.83"

\header {
  title = "Prelude"
  subtitle = "B Minor"
  composer = "Caleb Darling"
  copyright = "Copyright (c) 2019"
  tagline = ##f
}

\paper {
  #(set-paper-size "letter")
}




%globals

sr = \change Staff = "right"
sl = \change Staff = "left"






%section a (subject 1)

init = {
  \key b \minor
  \time 9/8
  \numericTimeSignature
  \accidentalStyle "piano"
  \tempo "Allegro Agitato" 4.=100
}


%section aa (mainly rh/lh doubling)

aa_undoubled = {
  \repeat unfold 2 {
    b16 d fis b fis d   b[ d fis b fis]   d[ f bes d bes f d] |
  }
  g bes d g d bes   es, g bes es bes g   c, es g c g es |
  d fis a d a fis   d fis a c a fis
  \tag #'right {d fis b d b fis |}
  \tag #'left {b, d fis b fis d |}
  \repeat unfold 2 {
    b d fis b fis d   b[ d fis b fis]   d[ f bes d bes f d] |
  }
  g bes d g d bes   fis bes d fis d bes   f bes d f d bes |
  \repeat unfold 2 {fis bes d fis d bes}
  \tag #'right {bes,,}
  \tag #'left {bes,}
  d fis bes fis d |
  b d fis b fis d   bes d fis bes fis d   a d fis a fis d |
  \repeat unfold 3 {aes d fis aes fis d} |
}

aa_melody = {
	b8 s4 b8 s8 s16 d4 s8. 
	|
	b8 s4 b8 s8 s16 d4 s8. 
	|
	g8 s4 es8 s4 c8 s4
	|
	d8 s4 d8 s4 
	\tag #'right {d8 s4 | b'8}
	\tag #'left {b8 s4 | b8}
	s4 b8 s8 s16 d4 s8. 
	|
	b8 s4 b8 s8 s16 d4 s8. 
	|
	g8 s4 fis8 s4 f8 s4
	|
	fis8 s4 fis8 s4 
	\tag #'right {bes,,8}
	\tag #'left {bes,8}
	s4
	|
	b8 s4 bes8 s4 a8 s4
	|
	aes8 s4 aes8 s4 aes8 s4
	|
}

aa_dynamics = {
  s1\p s8 |
  s1\< s8 |
  s1\mp s8 | s1\> s8 |
  s1\p s8 |
  s1\< s8 |
  s1\mf s8 |
  s4. s4. s4.\p |
  s1 s8 | s1\< s8 |
}


%section ab (lh octaves)

ab_right = {
  \repeat unfold 2 {aes16 d fis aes fis d}
  aes d fis aes d fis |
  \repeat unfold 3 {aes fis d aes d fis} |
  \repeat unfold 5 {aes d fis aes fis d} <aes aes'>4-. r8 |
}

ab_left = {
  <aes aes,>2 r16 <g g,>8. <bes bes,>4 <aes aes,>8 |
  <f f,>2 r16 <e e,>8. <g g,>4 <f f,>8 |
  <d d,>2 r16 <cis cis,>8. <e e,>4 <d d,>8 |
  <cis cis,>1 r8 |
}

ab_dynamics = {
  s1\f s8 |
  \repeat unfold 2 {s1 s8 |}
  s4.\< s4. s4.\! |
}


%combine aa and ab

a_right = \relative c' {
	<<
		\keepWithTag #'right \aa_undoubled
	\\
		\keepWithTag #'right \aa_melody
	>>
  \ab_right
}

a_left = \relative c {
	<<
		\keepWithTag #'left \aa_undoubled
	\\
		\keepWithTag #'left \aa_melody
	>>
  \ab_left
}

a_dynamics = {
  \aa_dynamics
  \ab_dynamics
}






%section b (subject 1 inversion [sort of])


b_right_repeated = {
  d'16-- a fis d fis a   c-- a fis d fis a   d-- b fis d fis d' |
  \repeat unfold 2 {
    b fis d b d fis   b fis d b d   d'[ bes f d f bes d] |
  }
  g-- d bes g bes d   es-- bes g es g bes   c-- g es c es g |
}

b_right = \relative c' {
  \repeat unfold 2 {\b_right_repeated}
  bes-- f d bes d f   a-- e cis a cis e   aes-- es c aes c es |
  g d b g b d   g d b g b   aes'[ es c aes c es aes] |
  b g d b d g   b g d b d   c'[ aes es c es aes c] |
  d b g d g b   d b g d g   es'[ c aes es aes c es] |
  g,-- d b g b d   aes'-- es c aes c es   ais-- fis cis ais cis fis |
  \repeat unfold 2 {
    b fis d b d fis   b fis d b d   d'[ bes f d f bes d] |
  }
  g-- d bes g bes d   fis-- d bes fis bes d   f-- d bes f bes d |
  \repeat unfold 2 {fis-- d ais fis ais d}
  ais,---> d fis ais fis d |
  
 \bar "||"
}



b_left_undoubled = {
  d4 r8 fis4 r8 d4 r8 |

  b4 r8 fis4 r8 f4 r8 |
  b4 r8 fis4 r8 bes4 r8 |
  g'8 r16 f8 r16   es8 r16 d8 r16   c8 r16 bes8 r16 |
  a4 r8 d4 r8 b4 r8 |

  b,8 r16 d8 r16   fis8 r16 b8 r16   fis8 r16 d8 r16 |
  b8 r16 fis'8 r16   b8 r16 d8 r16   f,8 r16 bes8 r16 |
  g8 r16 bes8 r16   es8 r16 g8 r16   c,8 r16 es8 r16 |
  bes8 r16 d8 r16   a8 r16 cis8 r16   aes8 r16 c8 r16 |
  
  \repeat unfold 3 {
    g8 r16 b8 r16   d8 r16 g8 r16   aes,8 r16 c8 r16 |
  }
  g8 r16 b8 r16   d8 r16 g8 r16   ais,8 r16 d8 r16 |

  b,8 r16 d8 r16   fis8 r16 b8 r16   fis8 r16 d8 r16 |
  b8 r16 fis'8 r16   b8 r16 d8 r16   f,8 r16 bes8 r16 |
  g8 r16 a8 r16   fis8 r16 gis8 r16   eis8 r16 fisis8 r16 |
  d8 r16 fis8 r16   ais8 r16 d8 r16   fis4.-> |
}

b_left = {
  <<
    \relative c {\b_left_undoubled}
    \relative c, {\b_left_undoubled}
  >>
}



b_dynamics = {
  s1\mp\> s8 |
  s1\p s8 |
  s1\< s8 |
  s1\mp s8 |
  s1\> s8 |
  s1\p s8 |
  s1\< s8 |
  s1\mf s8 |
  s1\> s8 |
  s1\pp s8 |
  \repeat unfold 2 {s1 s8 |}
  s1\< s8 |
  s1\mf s8 |
  s1 s8 |
  s1\f s8 |
  s1\< s8 |
}






%section c (subject 2, build up)


c_init = {
  \time 2/4
}

c_right_one = \relative c' {
  <fis d b fis>4-> s8 fis( |
  g4 g |
  \repeat unfold 2 {
    fis4. fis8 | g4 g |
  }
  g4. g8 | fis4 fis |
  fis4. fis8 | g4 g |
  fis4. fis8 | b4 b) |
  ais16--( fis cis ais)   cis'--( ais fis cis) |
  fis'--( cis ais fis)   ais'--( fis cis ais) |
}

c_right_two = \relative c' {
  s4   b16 d fis d |
  \repeat unfold 2 {
    g d bes g   g' d bes g |
    fis' d b fis    b d fis d |
  }
  g d b g   g' d b g |
  g' es bes g   bes es g es |
  fis d a fis   fis' d a fis |
  fis' d b fis   b d fis d |
  g d bes g   g' d bes g |
  fis' d b fis   b d fis d |
  b' g d b   b' g d b |
  s2*2
}

c_right = {
  \c_init
  << \c_right_one \\ \c_right_two >>
}



c_left = \relative c {
  <b b,>4-> b | bes, bes' |
  b, b' | bes, bes' |
  b, b' | g, g' |
  es es' | d, d' |
  <b, b,> <b' b,> |
  <bes, bes,> <bes' bes,> |
  <b, b,> <b' b,> |
  <g g,> <g' g,> |
  <<
    {s4 <fis cis ais fis>~ | <fis cis ais fis>2}
    \\
    {<fis, fis,>2~ | <fis fis,>}
  >>
}



c_dynamics = {
  s4\ff s4\pp |
  \override DynamicTextSpanner.style = #'none
  s2*7 | s2\cresc
  s2*3 | s2\<
  s2
}





%section d (subject 3)


d_right_outer = {
  \time 5/4
  b8->[ b, cis d]   r b[ cis d g-- fis--] |
  r b[ cis d]   r b[ cis d e-- cis--] |
  \time 4/4
  \clef bass
  r b,, cis d   
  \clef treble
  r b' cis d |
  r b' cis d   e fis g gis |
  a a b b   cis[ cis d] g, |
  fis4
}

d_right = {
  \new Voice <<
    \relative c''' {\d_right_outer}

    \relative c'' {
      fis8 fis, gis a   r fis gis a d cis |
      r fis gis a   r fis gis a b ais |
      r fis,, gis a   r fis' gis a |
      s^\markup{\italic{"precip."}} fis' gis a   b cis d dis |
      e e fis fis   gis gis a
    }

    \relative c'' {
      d8 d, e fis   r d e fis b ais |
      r d e fis   r d e fis gis fis |
      r d,, e fis   r d' e fis |
      r d' e fis   gis a b b |
      c c d d   e e fis
    }

    \relative c'' {\d_right_outer}
  >>

  \relative c''''' {
    r8
    \ottava #1
    <g g,> <fis fis,>2_\markup{\italic{"rit."}} |
    \tempo "Adagio espressivo"
    <fis fis,> <e fis,> |
    \ottava #0
    \pageBreak
  }
}



d_left_undoubled = {
  b8->[ b' bes a]   b,--[ b' bes a eis-- fis--] |
  b,--[ b' bes a]   b,--[ b' bes a e-- fis--] |
  b,-- b' bes a   b,-- b' bes a |
  b,-> b' bes a   e es d cis |
  c c b b   bes[ bes a]
}

d_left = {
  \new Voice <<
    \relative c, \d_left_undoubled
    \relative c,, \d_left_undoubled
  >>

  \relative c,, {
    %\ottava #-1
    eis8[ | fis
    %\ottava #0
    fis' fis']
    %\ottava #-1
    eis,,[ fis
    %\ottava #0
    fis' fis' fis'] |
    \clef treble
    fis'1
  }
}

d_dynamics = {
  s8\sff s8\f\< s1 |
  s1\< s4 |
  s1\mf\< |
  s1\fff |
  s1*2 |
  s1\> |
}







%section e (subject 1 var, slow)

e_init = {
  \time 6/16
  \override Beam.beam-thickness = #.4
  \override Beam.gap = #.1
}

e_melody = \relative c'' {
  d4(\p   fis16 b |
  d8.  cis16 b fis |
  f4   cis'16 d |
  f8.[   d)] |
  
  d,4(   fis16 b |
  d8.  cis16 b fis |
  f4   cis'16 d |
  f8.[   d)] |
  
  bes4(\mp   a16 gis |
  g4   fis16 e |
  es4. | d) |
  \pageBreak
  \ottava #1
  bes''4(\pp   a16 gis |
  g4   fis16 e |
  es4. | d)-\fermata_\markup{\italic{"rit."}} |
  \ottava #0
  \bar "||"
}

bminor_arpeggio = {
  b32 d fis b
  \sr
  \tag #'b {d fis b fis}
  \tag #'cis {d fis cis' fis,}
  \sl
  d b fis d
}

besmajor_arpeggio = {
  d32 f bes d
  \sr
  f bes d bes
  \sl
  f d bes f
}

gminor_arpeggio = {
  g32 bes d g
  \sr
  bes d g d
  \sl
  g, d bes g
}

esmajor_arpeggio = {
  es32 g bes es
  \sr
  g bes es bes
  \sl
  es, bes g es
}

cminor_arpeggio = {
  c32 es g c
  \sr
  es g c g es
  \sl
  c g es
}

dmajor_arpeggio = {
  d32 fis a d
  \sr
  fis a c a fis
  \sl
  d a fis
}

e_arpeggios = {
  \repeat unfold 2 {
    \clef bass
    \relative c {
      \keepWithTag #'b \bminor_arpeggio |
    }
    \clef treble
    \relative c' {
      \keepWithTag #'cis \bminor_arpeggio |
    }
    \clef bass
    \relative c {
      \besmajor_arpeggio |
    }
    \clef treble
    \relative c' {
      \besmajor_arpeggio |
    }
  }
  \relative c' {
    \gminor_arpeggio |
    \clef bass
    \esmajor_arpeggio |
    \cminor_arpeggio |
    \dmajor_arpeggio |
  }
  \clef treble
  \relative c'' {
    \gminor_arpeggio |
    \esmajor_arpeggio |
    \cminor_arpeggio |
    \dmajor_arpeggio |
  }
}

e_right = {
  \e_init
  
  \stemUp
  \slurUp
  \mergeDifferentlyDottedOn
  \e_melody
  \stemNeutral
  \slurNeutral
  \mergeDifferentlyDottedOff
}

e_left = {
  \e_init
  \e_arpeggios
}

e_dynamics = {
  s4.\!
  s4.*15
}







% section f (subject 1', fast


f_init = {
  \time 6/8
  \tempo "Tempo primo"
}

cb = \clef bass

ct = \clef treble

f_right = \relative c'' {
  \f_init
  \stemUp
  \slurUp
  d4.\f \cb d,,-> |
  \ct f'' \cb f,,-> |
  \ct d''4. \cb d,,-> |
  \ct f'' \cb f,,-> |
  \ct bes''( g |
  es  d) |
  \pageBreak
  bes( bes |
  ais^\< fis') |
  \time 9/8
  fis(_\ff fis e | 
  e d d |
  cis cis b |
  \pageBreak
  b b ais) |
  \stemNeutral
  \slurNeutral
}

fast_bminor_arpeggio = {
  b32 d fis b \sr d fis b fis d \sl b fis d
}

fast_besmajor_arpeggio = {
  d32 f bes d \sr
  f bes d bes f \sl d bes f
}

f_left = {
  \clef bass
  \repeat unfold 2 {
    \relative c { \fast_bminor_arpeggio }
    \ottava #-1
    \relative c,, { \fast_bminor_arpeggio }
    \ottava #0
    \relative c { \fast_besmajor_arpeggio }
    \ottava #-1
    \relative c,, { \fast_besmajor_arpeggio }
    \ottava #0
  }
  
  
  \relative c' {
  
    %g minor
    g32 bes d g \sr bes d g d bes \sl g d bes
    
    %es major
    es, g bes es \sr g bes es bes g \sl es bes g |
    
    %c minor
    c, es g c \sr es g c g es \sl c g es
    
    %d major
    d fis a d \sr fis a c a fis \sl d a fis |
    
    %g minor
    g, bes d g \sr bes d g d bes \sl g d bes
    
    %bes major
    f bes d f \sr bes d f d bes \sl f d bes |
    
    %aug
    fis ais d fis \sr ais d fis d ais \sl fis d ais
    
    %aug2
    d fis ais d \sr fis ais d ais fis \sl d ais fis |
    
    \time 9/8
    
    %b minor
    d fis b d \sr fis b d b fis \sl d b fis
    
    %e minor
    e g b e \sr g b e-- b g \sl e b g
    
    %aug
    d fis ais d \sr fis ais d-- ais fis \sl d ais fis 
    
    |
    
    %b minor
    d fis b d \sr fis b d-- b fis \sl d b fis
    
    %e minor
    b, e g b \sr e g cis-- g e \sl b g e
    
    %fis major
    cis fis ais cis \sr fis ais cis-- ais fis \sl cis ais fis 
    
    |
    
    %b minor
    b, d fis b \sr d fis b-- fis d \sl b fis d
    
    %e minor
    b e g b \sr e g b-- g e \sl b g e
    
    %fis major
    ais, cis fis ais \sr cis fis ais-- fis cis \sl ais fis cis
    
    |
    
    %b minor
    fis, b d fis \sr b d fis d b \sl fis d b
    
    %g major
    g b d g \sr b d g d b \sl g d b
    
    %fis major
    fis ais cis fis \sr ais cis fis cis ais \sl fis cis ais |
  }
}

f_dynamics = {
  s2. |
  s2.*7 |
  s1 s8 |
  \repeat unfold 3 {s1 s8} |
}







% section g (subject 2')
g_init = {
  \time 2/4
}

g_right_one = \relative c'' {
  <fis d b fis>4-> s8 fis8( | g4 g | 
  fis4. fis8 | g4 g |
  fis4. fis8 | g4 g |
  g4. g8 | fis4 fis |
  fis,4. fis8 | g4 g |
  fis4. fis8 | b4 b |
  b4. b8 | bes4 bes |
  \pageBreak
  a4. a8 | aes4 aes |
  gis4. gis8 | g4 bes) |
}

g_right_two = \relative c'' {
  s4   b16 d fis d |
  g d bes g   g' d bes g |
  fis' d b fis   b d fis d |
  g d bes g   g' d bes g |
  fis' d b fis   b d fis d |
  g d b g   g' d b g |
  g' es bes g   bes es g es |
  fis d a fis   fis' d a fis |
  fis d b fis   b d fis d |
  g d bes g   g' d bes g |
  fis' d b fis   b d fis d |
  b' g d b   b' g d b |
  b' gis dis b   dis gis b gis |
  bes g d bes   bes' g d bes |
  a' fis cis a   cis fis a fis |
  aes f c aes   aes' f c aes |
  gis' e b gis   b e gis e |
  g es bes g   bes' g es bes |
  \break
}

g_right = {
  \g_init
  << \g_right_one \\ \g_right_two >>
}



g_left_outer = {
  b4-> d | cis fis, |
  b d | cis fis, |
  b d | g8 d b g |
  es4-- es' | d,-- d' |
  b d | cis fis, |
  b d | g,8 b d g |
  f,4-- f' | e,-- e' |
  es,-- es' | d,-- d' |
  cis,-- cis' | es,-- es' |
  
}

g_left_inner = \relative c {
  s2*12 |
  s4 <c aes> |
  s4 <b g> |
  s4 <bes ges> |
  s4 <a f> |
  s4 <gis e> |
  s4 <bes g> |
}

g_left = {
  \g_init
  \new Voice <<
    \relative c { \g_left_outer }
    \g_left_inner
    \relative c, { \g_left_outer }
  >>
}

g_dynamics = {
  s4\sff s4\f |
  s2*7 |
  s2\p
  s2*2 |
  s2\< |
  s2\mf |
  s2*3 |
  s2\p |
  s2\>
}








% section h (subject c')

h_init = {
  \time 4/4
}

h_right_single = {
  \repeat unfold 2 {
    r8 gis ais b   r gis' ais b |
    es d cis c   es, d cis c |
  }
}

h_right = {
  \h_init
  
  \new Voice <<
    \relative c'' \h_right_single
    \transpose gis dis \relative c'' {\h_right_single }
    \transpose gis b \relative c' { \h_right_single }
    \relative c' \h_right_single
  >>
  \relative c' {
    r8 <d a f d> <e b g e> <f c a f>
    r8 <d' a f d> <e b g e> <f c a f> |
    r8 <d' a f d> <e b g e> <f c a f>
    <g d b g> <g d b g> <fis cis ais fis> <fis cis ais fis> |
  }
}



h_left = \relative c {
  \h_init
  
  \repeat unfold 6 {
    <gis dis b gis>8 <fis' gis,> <f g,> <es fis,> 
  }
  \repeat unfold 4 {
    <d, a f d> <c' d,> <b cis,> <a bis,>
  }
  <d, a f d>-> <d' d,> <cis cis,> <bis bis,>
  <g g,> <g g,> <fis fis,> <fis fis,> |
}

h_dynamics = {
  s1\pp |
  s1*4 |
  s1\f\< |
}





% final

i_right_one = \relative c''' {
  \time 3/2
  \stemDown
  <fis d b fis>8-> 
  \stemNeutral
  <fis,, b,>([ d b])
  fis'4. e8
  d( cis d e) |
  \time 4/2
  <fis b,>4( d8 b )
  fis'4. e8(
  <d ais fis>2 <cis ais fis>) |
}

i_right_two = \relative c {
  \time 3/2
  s8 fis4. <b g>2 ais |
  \time 4/2
  fis <b g> s1 |
}

i_right = {
  << \i_right_one \\ \i_right_two >>
  \relative c' {
    \slurUp
    <bes f d>1( <b fis d>)
  }
}

i_left_undoubled = {
  \time 3/2
  b8-> b'4. e,2 fis |
  \time 4/2
  b e, fis1 |
  b,\breve
}

i_left = {
  \new Voice <<
    \relative c, \i_left_undoubled
    \relative c,, \i_left_undoubled
  >>
}

i_dynamics = {
  s1.\ff |
  s1 s1\> |
  s1\pp s1 |
}







% put it all together


right = \new Voice {
  \init

  \a_right
  \b_right
  \c_right
  \d_right
  \e_right
  \f_right
  \g_right
  \h_right
  \i_right
  
  \bar "|."
}

left = {
  \init
  \clef bass

  \a_left
  \b_left
  \c_left
  \d_left
  \e_left
  \f_left
  \g_left
  \h_left
  \i_left
}

dynamics = {
  \a_dynamics
  \b_dynamics
  \c_dynamics
  \d_dynamics
  \e_dynamics
  \f_dynamics
  \g_dynamics
  \h_dynamics
  \i_dynamics
}

\score {
  \new PianoStaff \with {
    \override VerticalAxisGroup.staff-staff-spacing = 
      #'((stretchability . 50))
  }
  <<
    \new Staff = "right" \right
    \new Dynamics \dynamics
    \new Staff = "left" {\clef bass \left}
  >>
  \layout { 

  }
  \midi { }
}
