This folder contains an updated and an original score for Prelude in B Minor, which I wrote around 2009-2010. When there were discrepancies with what was written and the way I play the piece, I put down the way I play it in the edited score. The "original" score was itself edited
sometime after I composed the piece, and I think how I play it now is closer to how the piece originally was.

You can listen to a (not so great) recording [here](https://soundcloud.com/user-337520672/prelude-1).

prelude_b_minor.ly -- Lilypond file to generate the score, look if you dare

prelude_b_minor.mid -- Midi file lilypond produces

prelude_b_minor.pdf -- The score

prelude_b_minor_original.pdf -- The old score I used. Provided only for legacy purposes

All files are released under CC-BY-SA 4.0. Basically you can do whatever you want with these files and their contents,
perform these works, distribute the files, and even edit or transform this work in any way, as long as you give appropriate credit
and release any changes you make under the same license. The full license is available [here](https://creativecommons.org/licenses/by-sa/4.0/)