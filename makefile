lily = lilypond
lilypub = lilypond -dno-point-and-click
piece = prelude_b_minor

%.pdf: %.ly
	$(lily) $<

$(piece).pdf: $(piece).ly

.PHONY: publish
publish:
	$(lilypub) $(piece)
	mv $(piece).pdf ~/scores/pdfs/

.PHONY: clean
clean:
	rm *.pdf
